package com.socialmedia.controllers.api.v1;

import com.socialmedia.domain.CommentContainer;
import com.socialmedia.services.v1.jpainterface.CommentContainerCRUDService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(CommentContainerControllerAPI.BASE_URL)
public class CommentContainerControllerAPI {
    public static final String BASE_URL = "/api/v1/commentContainer";
    private CommentContainerCRUDService commentContainerCRUDService;

    public CommentContainerControllerAPI(CommentContainerCRUDService commentContainerCRUDService) {
        this.commentContainerCRUDService = commentContainerCRUDService;
    }

    @GetMapping("/{id}")
    public CommentContainer getCommentContainerById(@PathVariable Long id) {
        return commentContainerCRUDService.findById(id);
    }

    @GetMapping
    public List<CommentContainer> getCommentContainers() {
        return commentContainerCRUDService.findAll();
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CommentContainer updateCommentContainer(@PathVariable Long id, @RequestBody CommentContainer commentContainer) {
        return commentContainerCRUDService.update(id, commentContainer);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public CommentContainer saveCommentContainer(@RequestBody CommentContainer commentContainer) {
        return commentContainerCRUDService.save(commentContainer);
    }
}
