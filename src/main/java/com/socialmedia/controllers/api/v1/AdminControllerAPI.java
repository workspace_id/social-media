package com.socialmedia.controllers.api.v1;

import com.socialmedia.domain.Admin;
import com.socialmedia.services.v1.jpainterface.AdminCRUDService;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@CrossOrigin
@RequestMapping(AdminControllerAPI.BASE_URL)
public class AdminControllerAPI {
    public static final String BASE_URL = "/api/v1/admin";
    private AdminCRUDService adminCRUDService;

    public AdminControllerAPI(AdminCRUDService adminCRUDService) {
        this.adminCRUDService = adminCRUDService;
    }

    @GetMapping("/{id}")
    //PreAuthorize can be used instead of .antMatchers
    //You just need to add @EnableGlobalMethodSecurity(prePostEnabled = true)
    //In the ApplicationSecurityConfig
    @PreAuthorize("hasAuthority('student:read')")
    public Admin getAdminById(@PathVariable Long id) {
        return adminCRUDService.findById(id);
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_ADMINTRAINEE')")
    public List<Admin> getAdmins() {
        return adminCRUDService.findAll();
    }

    @PutMapping(value = "/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_ADMINTRAINEE')")
    @ResponseStatus(HttpStatus.OK)
    public Admin updateAdmin(@PathVariable Long id, @RequestBody Admin admin) {
        return adminCRUDService.update(id, admin);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_ADMINTRAINEE')")
    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public Admin saveAdmin(@RequestBody Admin admin) {
        return adminCRUDService.save(admin);
    }
}
