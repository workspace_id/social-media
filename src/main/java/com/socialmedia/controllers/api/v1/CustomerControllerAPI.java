package com.socialmedia.controllers.api.v1;

import com.socialmedia.domain.Customer;
import com.socialmedia.services.v1.jpainterface.CustomerCRUDService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(CustomerControllerAPI.BASE_URL)
public class CustomerControllerAPI {
    public static final String BASE_URL = "/api/v1/customers";
    private CustomerCRUDService customerCRUDService;

    public CustomerControllerAPI(CustomerCRUDService customerCRUDService) {
        this.customerCRUDService = customerCRUDService;
    }

    @GetMapping("/{id}")
    public Customer getCustomerById(@PathVariable Long id) {
        return customerCRUDService.findById(id);
    }

    @GetMapping
    public List<Customer> getCustomers() {
        return customerCRUDService.findAll();
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Customer updateCustomer(@PathVariable Long id, @RequestBody Customer customer) {
        return customerCRUDService.update(id, customer);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public Customer saveCustomer(@RequestBody Customer customer) {
        return customerCRUDService.save(customer);
    }
}
