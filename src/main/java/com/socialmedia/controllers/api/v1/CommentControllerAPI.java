package com.socialmedia.controllers.api.v1;

import com.socialmedia.domain.Comment;
import com.socialmedia.services.v1.jpainterface.CommentCRUDService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(CommentControllerAPI.BASE_URL)
public class CommentControllerAPI {
    public static final String BASE_URL = "/api/v1/comment";
    private CommentCRUDService commentCRUDService;

    public CommentControllerAPI(CommentCRUDService commentCRUDService) {
        this.commentCRUDService = commentCRUDService;
    }

    @GetMapping("/{id}")
    public Comment getCommentById(@PathVariable Long id) {
        return commentCRUDService.findById(id);
    }

    @GetMapping
    public List<Comment> getComments() {
        return commentCRUDService.findAll();
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Comment updateComment(@PathVariable Long id, @RequestBody Comment comment) {
        return commentCRUDService.update(id, comment);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public Comment saveComment(@RequestBody Comment comment) {
        return commentCRUDService.save(comment);
    }
}
