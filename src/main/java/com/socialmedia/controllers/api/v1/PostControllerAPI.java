package com.socialmedia.controllers.api.v1;

import com.socialmedia.domain.Post;
import com.socialmedia.services.v1.jpainterface.PostCRUDService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(PostControllerAPI.BASE_URL)
public class PostControllerAPI {
    public static final String BASE_URL = "/api/v1/post";
    private PostCRUDService postCRUDService;

    public PostControllerAPI(PostCRUDService postCRUDService) {
        this.postCRUDService = postCRUDService;
    }

    @GetMapping("/{id}")
    public Post getPostById(@PathVariable Long id) {
        return postCRUDService.findById(id);
    }

    @GetMapping
    public List<Post> getPosts() {
        return postCRUDService.findAll();
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Post updatePost(@PathVariable Long id, @RequestBody Post post) {
        return postCRUDService.update(id, post);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public Post savePost(@RequestBody Post post) {
        return postCRUDService.save(post);
    }
}
