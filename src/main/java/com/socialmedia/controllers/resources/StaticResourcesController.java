package com.socialmedia.controllers.resources;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class StaticResourcesController {

    //For static resources
    @GetMapping("/login")
    public String getLoginView() {
        return "login.html";
    }

    @GetMapping("/wall")
    public String getWall() {
        return "wall.html";
    }

}
