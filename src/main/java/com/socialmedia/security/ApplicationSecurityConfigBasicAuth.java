package com.socialmedia.security;

import com.socialmedia.security.domain.ApplicationUserPermission;
import com.socialmedia.security.domain.ApplicationUserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

/*@Configuration
@EnableWebSecurity
//In order to use annotation based security
@EnableGlobalMethodSecurity(prePostEnabled = true)*/
//FakeApplicationUserDAO and ApplicationUserService must be commented out in order for this to work
public class ApplicationSecurityConfigBasicAuth extends WebSecurityConfigurerAdapter {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public ApplicationSecurityConfigBasicAuth(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    /*Spring generates a default user - user
    Also generates a default password - from console

    Basic authentication - not form based authentication
    A request without username and password is sent - it is rejected - 401 unauthorized
    A request with username and password is sent - it is approved (if the username and password are correct)
    With basic authentication you cannot log out
    */

    //For Basic Authentication
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable() //Cross site request forgery - forging a request/website

                /*For more information on CSRF tokens:
                .csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())

                Cookie will not be accessible to client-side scripts
                If the client tries to access any cookies with some JS then he will be able to do so

                You can look at the CookieCsrfTokenRepository for more information
                .and() //Adding this when using with .withHttpOnlyFalse()

                To look at the cookie and how it works
                Spring Security manages this instead for us
                You can look at CSRFFilter Class in order to find out more
                There is information about how tokens are generated and compared

                Attackers forge Http requests and make them look like links
                They are sent to users
                A user that is logged in to the website, might click on the link
                This sends the forged Http request from a logged in valid user
                If CSRF is disabled, this request may pass as valid

                When enabled and the front-end sends requests to the back-end
                The back-end sends back a CSRF token to the front-end
                This token is used when sending POST, PUT, DELETE Http requests are sent
                The server validates the token when it is sent back with the form
                This defends users from CSRF attacks

                CSRF should be enabled for normal users using the API trough a browser
                CSRF should not be enabled for systems, interacting with the API (not browser based)

                CSRF is sent as a cookie token
                You can look at the actual token using Interceptor in Postman
                You need to have the CSRF cookie token to send POST, PUT, DELETE requests with CSRF enabled
                You need to define it as a header in your request as X-XSRF-TOKEN and its value

                 */

                .authorizeRequests() //We want to authorize requests
                .antMatchers("/", "index", "/css/*", "/js/*") //These urls
                .permitAll() //Are whitelisted for all users

                //Role based authentication
                .antMatchers("/api**") //Pattern
                .hasRole(ApplicationUserRole.STUDENT.name()) //Only STUDENT will have access to this page

                //Authority based authentication
                .antMatchers(HttpMethod.DELETE, "/api/v1/**")
                .hasAuthority(ApplicationUserPermission.COURSE_WRITE.getPermission())
                .antMatchers(HttpMethod.POST, "/api/v1/**")
                .hasAuthority(ApplicationUserPermission.COURSE_WRITE.getPermission())
                .antMatchers(HttpMethod.PUT, "/api/v1/**")
                .hasAuthority(ApplicationUserPermission.COURSE_WRITE.getPermission())

                //Role based authentication
                .antMatchers(HttpMethod.GET, "/api/v1/**")
                .hasAnyRole(ApplicationUserRole.ADMIN.name(), ApplicationUserRole.ADMINTRAINEE.name())

                .anyRequest() //Any (other) request
                .authenticated() //Must be authenticated (using username and password)
                .and() //And the mechanism we want to use to authenticate is
                .httpBasic(); //HttpBasic authentication
    }

    //Using in memory user details service - for the purpose of quickly adding some users
    @Override
    @Bean
    protected UserDetailsService userDetailsService() {

        //Users are UserDetails
        UserDetails annaSmithUser = User.builder()
                //All users should have a different username or else there is an exception
                .username("annasmith")
                //.password("password") //Password encoder is required, so cannot set password this way
                .password(passwordEncoder.encode("password")) //Actual password
                //Role based authentication
                .roles(ApplicationUserRole.STUDENT.name()) //Permissions are stored within the Enumerator
                //You can have multiple roles
                //Authentication using Authorities
                .authorities(ApplicationUserRole.STUDENT.getGrantedAuthority()) //These are the permissions the user has, like READ, WRITE of STUDENT and COURSES and etc.
                .build();

        UserDetails lindaUser = User.builder()
                .username("linda")
                .password(passwordEncoder.encode("password123"))
                .roles(ApplicationUserRole.ADMIN.name()) //This is ROLE_ADMIN
                .authorities(ApplicationUserRole.ADMIN.getGrantedAuthority())
                .build();

        UserDetails tomUser = User.builder()
                .username("tom")
                .password(passwordEncoder.encode("password123"))
                .roles(ApplicationUserRole.ADMINTRAINEE.name()) //This is ROLE_ADMINTRAINEE
                .authorities(ApplicationUserRole.ADMINTRAINEE.getGrantedAuthority())
                .build();

        //We use an InMemoryUserDetailsManager to store users in memory
        return new InMemoryUserDetailsManager(annaSmithUser, lindaUser, tomUser);
    }
}
