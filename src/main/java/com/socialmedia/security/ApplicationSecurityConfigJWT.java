package com.socialmedia.security;

import com.socialmedia.security.auth.database.services.impl.ApplicationUserService;
import com.socialmedia.security.auth.jwt.domain.JwtConfig;
import com.socialmedia.security.auth.jwt.services.JwtTokenVerifier;
import com.socialmedia.security.auth.jwt.services.JwtUsernameAndPasswordAuthenticationFilter;
import com.socialmedia.security.domain.ApplicationUserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.crypto.SecretKey;

@Configuration
@EnableWebSecurity
//In order to use annotation based security
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ApplicationSecurityConfigJWT extends WebSecurityConfigurerAdapter {

    private final PasswordEncoder passwordEncoder;
    private final ApplicationUserService applicationUserService;
    private final SecretKey secretKey;
    private final JwtConfig jwtConfig;

    @Autowired
    public ApplicationSecurityConfigJWT(PasswordEncoder passwordEncoder,
                                        ApplicationUserService applicationUserService,
                                        SecretKey secretKey,
                                        JwtConfig jwtConfig) {
        this.passwordEncoder = passwordEncoder;
        this.applicationUserService = applicationUserService;
        this.secretKey = secretKey;
        this.jwtConfig = jwtConfig;
    }
    /*Spring generates a default user - user
    Also generates a default password - from console

    Use localhost/8080/login to log in
    Use localhost/8080/logout to log out
    Order of methods called in this method is important




    For CSRF:
    .csrf().disable() //Cross site request forgery - forging a request/website

    For more information on CSRF tokens:
    .csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())

    Cookie will not be accessible to client-side scripts
    If the client tries to access any cookies with some JS then he will be able to do so

    //You can look at the CookieCsrfTokenRepository for more information

    To look at the cookie and how it works
    Spring Security manages this instead for us
    You can look at CSRFFilter Class in order to find out more
    There is information about how tokens are generated and compared

    Attackers forge Http requests and make them look like links
    They are sent to users
    A user that is logged in to the website, might click on the link
    This sends the forged Http request from a logged in valid user
    If CSRF is disabled, this request may pass as valid

    When enabled and the front-end sends requests to the back-end
    The back-end sends back a CSRF token to the front-end
    This token is used when sending POST, PUT, DELETE Http requests are sent
    The server validates the token when it is sent back with the form
    This defends users from CSRF attacks

    CSRF should be enabled for normal users using the API trough a browser
    CSRF should not be enabled for systems, interacting with the API (not browser based)

    CSRF is sent as a cookie token
    You can look at the actual token using Interceptor in Postman
    You need to have the CSRF cookie token to send POST, PUT, DELETE requests with CSRF enabled
    You need to define it as a header in your request as X-XSRF-TOKEN and its value

    It is best practice to use POST on any action that changes state (to defend against CSRF)
    */


    //For JWT Authentication
    //User session will not be stored
    //Adding filter for verification of the token after the log in verification?
    //For JWT

    //Each filter passes the request and response to the next one

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .addFilter(new JwtUsernameAndPasswordAuthenticationFilter(authenticationManager(), jwtConfig, secretKey))
                .addFilterAfter(new JwtTokenVerifier(secretKey, jwtConfig),JwtUsernameAndPasswordAuthenticationFilter.class)
                .authorizeRequests() //We want to authorize requests
                .antMatchers("/", "index", "/css/*", "/js/*")
                .permitAll() //Permit all users to access
                //If we add this before the .antMatchers with Http methods defined, all Http methods will be allowed
                .antMatchers("/api/**") //Pattern
                .hasRole(ApplicationUserRole.STUDENT.name()) //Role based authentication
                .anyRequest() //Any (other) request
                .authenticated(); //Must be authenticated (via username and password or JWT)
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(daoAuthenticationProvider());
    }

    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setPasswordEncoder(passwordEncoder);
        provider.setUserDetailsService(applicationUserService);
        return provider;
    }

}
