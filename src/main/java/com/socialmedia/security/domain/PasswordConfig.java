package com.socialmedia.security.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class PasswordConfig {

    @Bean
    public PasswordEncoder passwordEncoder() {
        //returning implementation of a PasswordEncoder:
        //BCryptPasswordEncoder a popular one

        return new BCryptPasswordEncoder(10);
    }
}
