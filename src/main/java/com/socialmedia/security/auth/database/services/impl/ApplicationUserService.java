package com.socialmedia.security.auth.database.services.impl;

import com.socialmedia.security.auth.database.services.ApplicationUserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

//For implementing usage of databases for user credentials storage
@Service
public class ApplicationUserService implements UserDetailsService {
    private final ApplicationUserDAO applicationUserDAO;


    @Autowired
    //Qualify the correct ApplicationUserDAO implementation - you may have multiple
    public ApplicationUserService(@Qualifier("fake") ApplicationUserDAO applicationUserDAO) {
        this.applicationUserDAO = applicationUserDAO;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return applicationUserDAO.selectApplicationUserByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("Username %s not found", username))
                );
    }
}
