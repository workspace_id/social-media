package com.socialmedia.security.auth.database.services;

import com.socialmedia.security.auth.database.domain.ApplicationUser;

import java.util.Optional;

public interface ApplicationUserDAO {

    Optional<ApplicationUser> selectApplicationUserByUsername(String username);


}
