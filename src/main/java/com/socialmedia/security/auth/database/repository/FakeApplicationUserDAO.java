package com.socialmedia.security.auth.database.repository;

import com.google.common.collect.Lists;
import com.socialmedia.security.auth.database.domain.ApplicationUser;
import com.socialmedia.security.auth.database.services.ApplicationUserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import static com.socialmedia.security.domain.ApplicationUserRole.*;

//In order to use a database, just implement ApplicationUserDAO and build this class for your DB and implement
//ApplicationUserDAO
@Repository("fake")
public class FakeApplicationUserDAO implements ApplicationUserDAO {
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public FakeApplicationUserDAO(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    public Optional<ApplicationUser> selectApplicationUserByUsername(String username) {
        return getApplicationUsers()
                .stream()
                .filter(applicationUser -> username.equals(applicationUser.getUsername()))
                .findFirst();
    }

    private List<ApplicationUser> getApplicationUsers() {
        List<ApplicationUser> applicationUsers = Lists.newArrayList(
                new ApplicationUser(
                        "annasmith",
                        passwordEncoder.encode("password123"),
                        STUDENT.getGrantedAuthority(),
                        true,
                        true,
                        true,
                        true
                ),


        new ApplicationUser(
                "linda",
                passwordEncoder.encode("password123"),
                ADMIN.getGrantedAuthority(),
                true,
                true,
                true,
                true
        ),

        new ApplicationUser(
                "tom",
                passwordEncoder.encode("password123"),
                ADMINTRAINEE.getGrantedAuthority(),
                true,
                true,
                true,
                true
        )
        );



        return applicationUsers;
    }


}
