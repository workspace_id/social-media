package com.socialmedia.security.auth.jwt.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.socialmedia.security.auth.jwt.domain.JwtConfig;
import com.socialmedia.security.auth.jwt.domain.UsernameAndPasswordAuthenticationRequest;
import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.crypto.SecretKey;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;

//Request filters perform validation
//There can be multiple filters
//The order of the filters is not guaranteed
//A request can pass trough multiple filters before getting to the api
public class JwtUsernameAndPasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;
    private final JwtConfig jwtConfig;
    private final SecretKey secretKey;

    public JwtUsernameAndPasswordAuthenticationFilter(AuthenticationManager authenticationManager,
                                                      JwtConfig jwtConfig,
                                                      SecretKey secretKey) {
        this.authenticationManager = authenticationManager;
        this.jwtConfig = jwtConfig;
        this.secretKey = secretKey;

    }






    //JWT Authentication is useful for when you need to have multiple different applications
    //Accessing your API
    //Like an iOS, Android, Web Application accessing your API

    //JSON Web Tokens are:
    //Fast
    //Stateless - you don't need to store any sessions in the database/in memory database
    //Used across many services

    //However, you have a:
    //Compromised secret key
    //No visibility to logged in users - cannot find if user is logged in or not
    //Token can be hijacked

    //The client sends a request with credentials
    //It is validated and a token is signed by the server and sent to the client
    //The token is sent with each request and validated again
    //The secret key is part of the JSON Web Token

    //JSON Web Tokens have a header, a payload and a verify signature

    //Client sends username and password in a POST request to the /login page and they are validated
    //A token is sent back if they are correct
    //The token can be used for every further request
    //It needs to be added to the Authorization tab in PostMan in order to use it
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws AuthenticationException {

        try {
            UsernameAndPasswordAuthenticationRequest authenticationRequest = new ObjectMapper()
                    .readValue(request.getInputStream(), UsernameAndPasswordAuthenticationRequest.class);


            //Authentication is an interface
            //UsernamePasswordAuthenticationToken is an implementation of the Authentication interface;
            Authentication authentication = new UsernamePasswordAuthenticationToken(
                    //Passing principle and credentials
                    authenticationRequest.getUsername(), //Principle
                    authenticationRequest.getPassword() //Credentials
            );

            //If username exists and the password for it is correct, the request is authenticated
            Authentication authenticate = authenticationManager.authenticate(authentication);
            return authenticate;

        } catch (IOException e) {
            throw new RuntimeException(e);
        }


    }



    //Generate and send token
    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {
        //This key should be long in order for it to be secure

        //Generate token
        String token = Jwts.builder()
                //linda or annasmith
                .setSubject(authResult.getName())
                .claim("authorities", authResult.getAuthorities())
                .setIssuedAt(new Date())
                //Expires in 10 days
                .setExpiration(java.sql.Date.valueOf(LocalDate.now().plusDays(jwtConfig.getTokenExpirationAfterDays())))
                .signWith(secretKey)
                .compact();







        //Send token
        response.addHeader(jwtConfig.getAuthorizationHeader(), jwtConfig.getTokenPrefix() + token);
    }
}