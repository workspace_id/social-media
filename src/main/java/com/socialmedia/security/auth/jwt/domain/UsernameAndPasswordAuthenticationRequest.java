package com.socialmedia.security.auth.jwt.domain;

import lombok.Data;

@Data
public class UsernameAndPasswordAuthenticationRequest {
    private String username;
    private String password;


    public UsernameAndPasswordAuthenticationRequest() {

    }
}
