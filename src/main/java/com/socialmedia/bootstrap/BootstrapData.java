package com.socialmedia.bootstrap;

import com.socialmedia.domain.Customer;
import com.socialmedia.repositories.CustomerRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class BootstrapData implements CommandLineRunner {
    private final CustomerRepository customerRepository;

    public BootstrapData(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Loading customer Data");

        Customer c1 = new Customer();
        c1.setFirstName("George");
        c1.setLastName("George");
        customerRepository.save(c1);

        Customer c2 = new Customer();
        c2.setFirstName("John");
        c2.setLastName("Peterson");
        customerRepository.save(c2);


        Customer c3 = new Customer();
        c3.setFirstName("Peter");
        c3.setLastName("Johnson");


        customerRepository.save(c3);
    }

}
