package com.socialmedia.repositories;

import com.socialmedia.domain.Post;
import org.springframework.data.jpa.repository.JpaRepository;

//AdminRepository interface to use all JpaRepository methods
public interface PostRepository extends JpaRepository<Post, Long> {
}
