package com.socialmedia.repositories;

import com.socialmedia.domain.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

//AdminRepository interface to use all JpaRepository methods
public interface AdminRepository extends JpaRepository<Admin, Long> {
}
