package com.socialmedia.repositories;

import com.socialmedia.domain.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

//AdminRepository interface to use all JpaRepository methods
public interface CommentRepository extends JpaRepository<Comment, Long> {
}
