package com.socialmedia.repositories;

import com.socialmedia.domain.Admin;
import com.socialmedia.domain.CommentContainer;
import org.springframework.data.jpa.repository.JpaRepository;

//AdminRepository interface to use all JpaRepository methods
public interface CommentContainerRepository extends JpaRepository<CommentContainer, Long> {
}
