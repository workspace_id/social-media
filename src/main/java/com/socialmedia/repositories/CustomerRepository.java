package com.socialmedia.repositories;

import com.socialmedia.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

//AdminRepository interface to use all JpaRepository methods
public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
