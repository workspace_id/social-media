package com.socialmedia.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Data
@Entity
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String url;
    private String firstName;
    private String lastName;
    private LocalDate dob;
    private LocalDate registeredOn;
    private String country;
    private String email;
    private String password;
}
