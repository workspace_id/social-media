package com.socialmedia.services.v1;

import java.util.List;

public interface CRUDService <T, ID> {
    T save(T t);
    T findById(ID id);
    List<T> findAll();
    List<T> deleteById(ID id);
    T update(ID id, T t);

}
