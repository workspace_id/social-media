package com.socialmedia.services.v1.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.socialmedia.domain.CommentContainer;
import com.socialmedia.repositories.CommentContainerRepository;
import com.socialmedia.services.v1.jpainterface.CommentContainerCRUDService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentContainerCRUDServiceImpl implements CommentContainerCRUDService {
    private final CommentContainerRepository commentContainerRepository;
    private final ObjectMapper objectMapper;

    public CommentContainerCRUDServiceImpl(CommentContainerRepository commentContainerRepository, ObjectMapper objectMapper) {
        this.commentContainerRepository = commentContainerRepository;
        this.objectMapper = objectMapper;
    }

    @Override
    public CommentContainer save(CommentContainer commentContainer) {
        return commentContainerRepository.save(commentContainer);
    }

    @Override
    public CommentContainer findById(Long id) {

        return commentContainerRepository.findById(id).get();
    }

    @Override
    public List<CommentContainer> findAll() {
        return commentContainerRepository.findAll();
    }

    @Override
    public List<CommentContainer> deleteById(Long id) {
        commentContainerRepository.deleteById(id);
        return commentContainerRepository.findAll();
    }

    @Override
    public CommentContainer update(Long id, CommentContainer commentContainer) {
        CommentContainer commentContainerEntry = commentContainerRepository.findById(id).get();
        commentContainerEntry = commentContainer;
        commentContainerEntry.setId(id);

        return commentContainerRepository.save(commentContainerEntry);
    }
}
