package com.socialmedia.services.v1.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.socialmedia.domain.Comment;
import com.socialmedia.repositories.CommentRepository;
import com.socialmedia.services.v1.jpainterface.CommentCRUDService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentCRUDServiceImpl implements CommentCRUDService {
    private final CommentRepository commentRepository;
    private final ObjectMapper objectMapper;

    public CommentCRUDServiceImpl(CommentRepository commentRepository, ObjectMapper objectMapper) {
        this.commentRepository = commentRepository;
        this.objectMapper = objectMapper;
    }

    @Override
    public Comment save(Comment comment) {
        return commentRepository.save(comment);
    }

    @Override
    public Comment findById(Long id) {

        return commentRepository.findById(id).get();
    }

    @Override
    public List<Comment> findAll() {
        return commentRepository.findAll();
    }

    @Override
    public List<Comment> deleteById(Long id) {
        commentRepository.deleteById(id);
        return commentRepository.findAll();
    }

    @Override
    public Comment update(Long id, Comment comment) {
        Comment commentEntry = commentRepository.findById(id).get();
        commentEntry = comment;
        commentEntry.setId(id);

        return commentRepository.save(commentEntry);
    }
}
