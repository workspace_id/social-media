package com.socialmedia.services.v1.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.socialmedia.domain.Post;
import com.socialmedia.repositories.PostRepository;
import com.socialmedia.services.v1.jpainterface.PostCRUDService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostCRUDServiceImpl implements PostCRUDService {
    private final PostRepository postRepository;
    private final ObjectMapper objectMapper;

    public PostCRUDServiceImpl(PostRepository postRepository, ObjectMapper objectMapper) {
        this.postRepository = postRepository;
        this.objectMapper = objectMapper;
    }

    @Override
    public Post save(Post post) {
        return postRepository.save(post);
    }

    @Override
    public Post findById(Long id) {

        return postRepository.findById(id).get();
    }

    @Override
    public List<Post> findAll() {
        return postRepository.findAll();
    }

    @Override
    public List<Post> deleteById(Long id) {
        postRepository.deleteById(id);
        return postRepository.findAll();
    }

    @Override
    public Post update(Long id, Post post) {
        Post postEntry = postRepository.findById(id).get();
        postEntry = post;
        postEntry.setId(id);

        return postRepository.save(postEntry);
    }
}
