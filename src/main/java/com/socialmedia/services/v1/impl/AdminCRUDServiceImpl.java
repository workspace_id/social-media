package com.socialmedia.services.v1.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.socialmedia.domain.Admin;
import com.socialmedia.repositories.AdminRepository;
import com.socialmedia.services.v1.jpainterface.AdminCRUDService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminCRUDServiceImpl implements AdminCRUDService {
    private final AdminRepository adminRepository;
    private final ObjectMapper objectMapper;

    public AdminCRUDServiceImpl(AdminRepository adminRepository, ObjectMapper objectMapper) {
        this.adminRepository = adminRepository;
        this.objectMapper = objectMapper;
    }

    @Override
    public Admin save(Admin admin) {
        return adminRepository.save(admin);
    }

    @Override
    public Admin findById(Long id) {

        return adminRepository.findById(id).get();
    }

    @Override
    public List<Admin> findAll() {
        return adminRepository.findAll();
    }

    @Override
    public List<Admin> deleteById(Long id) {
        adminRepository.deleteById(id);
        return adminRepository.findAll();
    }

    @Override
    public Admin update(Long id, Admin admin) {
        Admin adminEntry = adminRepository.findById(id).get();
        adminEntry = admin;
        adminEntry.setId(id);

        return adminRepository.save(adminEntry);
    }
}
