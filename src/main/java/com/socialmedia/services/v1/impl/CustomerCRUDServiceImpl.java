package com.socialmedia.services.v1.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.socialmedia.domain.Customer;
import com.socialmedia.repositories.CustomerRepository;
import com.socialmedia.services.v1.jpainterface.CustomerCRUDService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerCRUDServiceImpl implements CustomerCRUDService {
    private final CustomerRepository customerRepository;
    private final ObjectMapper objectMapper;

    public CustomerCRUDServiceImpl(CustomerRepository customerRepository, ObjectMapper objectMapper) {
        this.customerRepository = customerRepository;
        this.objectMapper = objectMapper;
    }

    @Override
    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public Customer findById(Long id) {

        return customerRepository.findById(id).get();
    }

    @Override
    public List<Customer> findAll() {
        return customerRepository.findAll();
    }

    @Override
    public List<Customer> deleteById(Long id) {
        customerRepository.deleteById(id);
        return customerRepository.findAll();
    }

    @Override
    public Customer update(Long id, Customer customer) {
        Customer customerEntry = customerRepository.findById(id).get();
        customerEntry = customer;
        customerEntry.setId(id);

        return customerRepository.save(customerEntry);
    }
}
