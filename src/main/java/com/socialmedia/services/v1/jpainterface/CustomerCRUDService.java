package com.socialmedia.services.v1.jpainterface;

import com.socialmedia.domain.Customer;
import com.socialmedia.services.v1.CRUDService;

public interface CustomerCRUDService extends CRUDService<Customer, Long> {
}
