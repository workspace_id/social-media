package com.socialmedia.services.v1.jpainterface;

import com.socialmedia.domain.Post;
import com.socialmedia.services.v1.CRUDService;

public interface PostCRUDService extends CRUDService<Post, Long> {
}
