package com.socialmedia.services.v1.jpainterface;

import com.socialmedia.domain.Admin;
import com.socialmedia.services.v1.CRUDService;

public interface AdminCRUDService extends CRUDService<Admin, Long> {
}
