package com.socialmedia.services.v1.jpainterface;

import com.socialmedia.domain.CommentContainer;
import com.socialmedia.services.v1.CRUDService;

public interface CommentContainerCRUDService extends CRUDService<CommentContainer, Long> {



}
