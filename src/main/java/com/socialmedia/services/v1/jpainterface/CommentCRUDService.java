package com.socialmedia.services.v1.jpainterface;

import com.socialmedia.domain.Comment;
import com.socialmedia.services.v1.CRUDService;

public interface CommentCRUDService extends CRUDService<Comment, Long> {
}
